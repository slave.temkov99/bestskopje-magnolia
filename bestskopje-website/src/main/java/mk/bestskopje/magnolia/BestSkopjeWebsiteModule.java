package mk.bestskopje.magnolia;

/**
 * This class is optional and represents the configuration for the BEST Skopje WEBSITE Magnolia Module module.
 * By exposing simple getter/setter/adder methods, this bean can be configured via content2bean
 * using the properties and node from <tt>config:/modules/BEST Skopje WEBSITE Magnolia Module</tt>.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 * See https://documentation.magnolia-cms.com/display/DOCS/Module+configuration for information about module configuration.
 */
public class BestSkopjeWebsiteModule {
    /* you can optionally implement info.magnolia.module.ModuleLifecycle */

}
