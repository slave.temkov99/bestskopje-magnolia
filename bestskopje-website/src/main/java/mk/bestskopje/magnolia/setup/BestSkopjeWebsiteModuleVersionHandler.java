package mk.bestskopje.magnolia.setup;

import com.google.common.collect.Lists;
import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.BootstrapSingleResource;
import info.magnolia.module.delta.Task;
import info.magnolia.module.model.Version;

import javax.jcr.ImportUUIDBehavior;
import java.util.List;

/**
 * This class is optional and lets you manage the versions of your module,
 * by registering "deltas" to maintain the module's configuration, or other type of content.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 *
 * @see info.magnolia.module.DefaultModuleVersionHandler
 * @see info.magnolia.module.ModuleVersionHandler
 * @see info.magnolia.module.delta.Task
 */
public class BestSkopjeWebsiteModuleVersionHandler extends DefaultModuleVersionHandler {

    /*
     * for initial module installation
     */
    @Override
    protected List<Task> getExtraInstallTasks(InstallContext installContext) {
        final List<Task> tasks = Lists.newArrayList();
        tasks.addAll(getDefaultTasks());
        return tasks;
    }

    /*
     * for each version upgrade
     */
    @Override
    protected List<Task> getDefaultUpdateTasks(Version forVersion) {
        final List<Task> defaultUpdateTasks = Lists.newArrayList();
        defaultUpdateTasks.addAll(getDefaultTasks());
        defaultUpdateTasks.addAll(super.getDefaultUpdateTasks(forVersion));
        return defaultUpdateTasks;
    }

    /**
     * List of tasks done on installation or on every version update
     * @return List of tasks
     */
    private List<Task> getDefaultTasks() {
        final List<Task> defaultUpdateTasks = Lists.newArrayList();

        //General site configuration
        defaultUpdateTasks.add(new BootstrapSingleResource("BEST Skopje site Configuration - bestskopje", "BEST Skopje site configuration - bestskopje", "/mgnl-bootstrap/versionhandler/multisite/config.modules.multisite.config.sites.bestskopje.yaml", ImportUUIDBehavior.IMPORT_UUID_COLLISION_REPLACE_EXISTING));
        defaultUpdateTasks.add(new BootstrapSingleResource("BEST Skopje site Configuration - fallback", "BEST Skopje site configuration - fallback", "/mgnl-bootstrap/versionhandler/multisite/config.modules.multisite.config.sites.fallback.yaml", ImportUUIDBehavior.IMPORT_UUID_COLLISION_REPLACE_EXISTING));

        //Content translation - custom commands
        defaultUpdateTasks.add(new BootstrapSingleResource("Content translation member download command", "Content translation member download command", "/mgnl-bootstrap/versionhandler/config/translation/config.modules.translation.commands.translation.downloadMembersTranslationFile.xml", ImportUUIDBehavior.IMPORT_UUID_COLLISION_REPLACE_EXISTING));
        defaultUpdateTasks.add(new BootstrapSingleResource("Content translation partner download command", "Content translation partner download command", "/mgnl-bootstrap/versionhandler/config/translation/config.modules.translation.commands.translation.downloadPartnersTranslationFile.xml", ImportUUIDBehavior.IMPORT_UUID_COLLISION_REPLACE_EXISTING));

        return defaultUpdateTasks;
    }
}