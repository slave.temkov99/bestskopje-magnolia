[#--------------- RENDERING ---------------]
<!DOCTYPE html>
<html xml:lang="${cmsfn.language()!}" lang="${cmsfn.language()!}">

<head>
    [@cms.page /]

    [@cms.area name="htmlHeader" /]
</head>

<body style="padding-top: 70px;">
    <div id="header">
        [@cms.area name="header" /]
    </div>

    [@cms.area name="main" /]

    <hr class="mt-5">
    [@cms.area name="footer" /]
</body>
</html>