[#--------------- ASSIGN ---------------]
[#assign page = cmsfn.page(content)! /]
[#assign rootPage = navfn.rootPage(page)! /]
[#assign navItems = navfn.navItems(rootPage)! /]
[#assign mainNavPage = navfn.ancestorPageAtLevel(page, 2)! /]
[#assign localizedLinks = cmsfn.localizedLinks()! /]
[#assign flags = {"mk":"https://www.worldatlas.com/img/flag/mk-flag.jpg", "en":"https://jooinn.com/images/union-jack-clipart-15.png"} /]

[#--------------- RENDERING ---------------]
<div class="container-fluid mt-1">
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top my-1 mx-lg-2 mx-1 rounded" style="background-color: #252530;">
        <a class="navbar-brand" href="${cmsfn.link(rootPage)!}">
            <img src="/.resources/bestskopje-website/webresources/images/best-skopje-logo.svg" width="250" height="100" alt="${i18n['bestskopje-website.global.bestskopjeLogo']}">
        </a>
        <button class="navbar-toggler" type="button" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item mx-3 text-center">
                    <a class="nav-link[#if page == rootPage] active[/#if]" href="${cmsfn.link(rootPage)!}">${i18n['bestskopje-website.global.homePage']}</a>
                </li>
                [#list navItems as navItem]
                    [#if navItem.@path?contains("error404")]
                        [#continue /]
                    [/#if]
                    <li class="nav-item mx-3 text-center">
                        <a class="nav-link[#if navItem == page || navItem==mainNavPage] active[/#if]" href="${cmsfn.link(navItem)!}">${navItem.navigationTitle!navItem.title!navItem.@name}</a>
                    </li>
                [/#list]
                [#list localizedLinks?keys as lang]
                    [#assign localizedLink = localizedLinks[lang] /]
                    <li class="nav-item mx-3 text-center">
                        <a class="nav-link" href="${localizedLink?remove_ending('.html')}">
                            <img src="${flags[lang]}" width="20" height="15">
                        </a>
                    </li>
                [/#list]

                [@cms.area name="callToAction" /]
            </ul>
        </div>
    </nav>
</div>