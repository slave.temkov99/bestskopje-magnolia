﻿[#--------------- ASSIGN ---------------]
[#assign page = cmsfn.page(content)! /]
[#assign rootPage = navfn.rootPage(page)! /]

[#--------------- RENDERING ---------------]

<div class="container-lg shadow-lg rounded-lg mt-5">
    <div class="row- mx-lg-5 mx-md-3 mx-sm-1 pt-3 text-center">
        <div class="h1">${content.caption!}
            <hr />
        </div>
    </div>
    <div class="row w-100">
        <div class="col-12 text-center">
            <p class="text-center">
            <hr>
            ${cmsfn.decode(content).text!}
            </p>
        </div>

        <a href="${cmsfn.link(rootPage)!}" class="text-light mt-3 mx-auto"><span class="button-decoration">${content.homePageButtonText!rootPage.navigationTitle!rootPage.Title!rootPage.@name}</span></a>
    </div>
</div>