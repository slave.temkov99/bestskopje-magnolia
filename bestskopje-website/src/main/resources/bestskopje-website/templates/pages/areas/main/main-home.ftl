[#--------------- RENDERING ---------------]
<div class="row mx-2 my-xl-5">
    <div class="col-lg-6 col-md-12 p-0">
        <a href="${content.externalLink!}" target="_blank">
            <img src="/.resources/bestskopje-website/webresources/images/main-homepage/best-int-logo.svg" alt="${i18n['bestskopje-website.global.best-int-logo']}" class="img-fluid landing-relative-parent" />
        </a>
    </div>
    <div class="col-lg-6 col-md-12 p-0">
        <img src="/.resources/bestskopje-website/webresources/images/main-homepage/best-int-map.svg" alt="${i18n['bestskopje-website.global.best-int-map']}" class="img-fluid landing-relative-parent" />
    </div>
</div>