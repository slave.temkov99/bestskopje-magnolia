﻿[#--------------- ASSIGN ---------------]
[#assign page = cmsfn.page(content)! /]

[#--------------- RENDERING ---------------]
<div class="container-lg shadow-lg rounded-lg mt-5">
    <div class="row- mx-lg-5 mx-md-3 mx-sm-1 pt-3 text-center">
        <div class="h1">${page.navigationTitle!page.title!page.@name}
            <hr />
        </div>
    </div>
    [@cms.area name="main-components" /]
</div>