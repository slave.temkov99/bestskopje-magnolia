[#--------------- RENDERING ---------------]
<footer class="container-fluid row align-items-center">
    <div id="logo" class="col-lg-2 col-md-6 col-sm-12 mb-md-0 mb-3 ">
        <img class="img-fluid" src="/.resources/bestskopje-website/webresources/images/footer/best-skopje-black-logo.svg">
    </div>

    [@cms.area name="generalContactInfo" /]

    [@cms.area name="contactPersons" /]

    <div class="row w-100">
        <div class="col-12 text-center">
            <p class="text-center">
            <hr>
            ${i18n['bestskopje-website.global.copyrightText']}
            </p>
        </div>

    </div>
</footer>