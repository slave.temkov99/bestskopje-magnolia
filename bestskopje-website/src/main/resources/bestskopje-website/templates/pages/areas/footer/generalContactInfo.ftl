[#--------------- ASSIGN ---------------]
[#assign links = ["website", "mailTo", "instagram", "facebook", "linkedIn"] /]

[#--------------- RENDERING ---------------]
<div id="Info" class="col-lg-4 col-md-6 col-sm-12 d-flex align-items-center flex-column mb-md-0 mb-3 ">
    <div class="d-flex align-items-center">
        <img id="pin" class="img-thumbnail m-3 border rounded p-2" src="/.resources/bestskopje-website/webresources/images/footer/location.svg">
        <p class="m-0">${content.location!}</p>
    </div>

    <div class="d-flex flex-row border p-3 align-items-center justify-content-around w-100 mt-3 rounded1_3">
        [#list links as link]
            [#if content[link + "Link"]?has_content]
                <a href="${content[link + 'Link']}"><img src="/.resources/bestskopje-website/webresources/images/footer/${link}.svg"></a>
            [/#if]
        [/#list]
    </div>

</div>