[#--------------- ASSIGN ---------------]
[#assign site = sitefn.site()! /]
[#assign theme = sitefn.theme(site)! /]

[#--------------- RENDERING ---------------]
<title>${content.title!content.@name}</title>
<meta charset="UTF-8">
<meta name="description" content="${content.description!}">
<meta name="keywords" content="${content.keywords!}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" href="/.resources/bestskopje-website/webresources/images/favicon.svg" />

[#list theme.jsFiles as jsFile]
    <script src="${jsFile.link!}"></script>
[/#list]

<script>
    $(function() {
        $("button.navbar-toggler").click(function(){
            $(".navbar-collapse").collapse("show");
            $(".navbar-collapse").collapse("hide");
        });
    });
</script>

<link href="http://fonts.cdnfonts.com/css/04b30" rel="stylesheet">
<link href="//db.onlinewebfonts.com/c/65ab651cc7cebf7331a4228e830573d1?family=Caviar+Dreams" rel="stylesheet" type="text/css"/>
[#list theme.cssFiles as cssFile]
    <link rel="stylesheet" href="${cssFile.link!}" />
[/#list]