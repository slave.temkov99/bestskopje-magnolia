[#--------------- ASSIGN ---------------]
[#assign contactMemberId = content.contactMember! /]
[#if contactMemberId?has_content]
    [#assign contactMember = cmsfn.contentById(contactMemberId, "members")! /]
    [#if contactMember?has_content]
        [#assign contactMemberInfoI18n = cmsfn.wrapForI18n(contactMember)! /]
    [/#if]
[/#if]

[#--------------- RENDERING ---------------]
[#if contactMemberInfoI18n?has_content]
    <div class="col-lg-3 col-md-6 col-sm-12 d-flex align-items-center flex-column mb-md-0 mb-3 ">

        <div class="ContactName">${contactMemberInfoI18n.personName!}</div>
        [#if (content.displayRole!false) && contactMemberInfoI18n.role?has_content]<p class="ContactRole">${contactMemberInfoI18n.role!}</p>[/#if]
        [#if ((content.displayNumber!false) && contactMemberInfoI18n.phoneNumber?has_content) || ((content.displayEmail!false) && contactMemberInfoI18n.emailAddress?has_content)]
            <div class="d-flex">
                <div class="d-flex flex-column align-items-end">

                    [#if (content.displayNumber!false) && contactMemberInfoI18n.phoneNumber?has_content]<img class="border rounded p-2 m-3" src="/.resources/bestskopje-website/webresources/images/footer/mobile.svg">[/#if]
                    [#if (content.displayEmail!false) && contactMemberInfoI18n.emailAddress?has_content]<img class="border rounded p-2 mx-3" src="/.resources/bestskopje-website/webresources/images/footer/mailTo.svg">[/#if]


                </div>
                <div class="d-flex flex-column align-items-start justify-content-around">

                    [#if (content.displayNumber!false) && contactMemberInfoI18n.phoneNumber?has_content]<span class=" mt-3">${contactMemberInfoI18n.phoneNumber!}</span>[/#if]
                    [#if (content.displayEmail!false) && contactMemberInfoI18n.emailAddress?has_content]<a href="mailto:${contactMemberInfoI18n.emailAddress!}"><span class=" mt-3">${contactMemberInfoI18n.emailAddress!}</span></a>[/#if]
                </div>
            </div>
        [/#if]
    </div>
[/#if]