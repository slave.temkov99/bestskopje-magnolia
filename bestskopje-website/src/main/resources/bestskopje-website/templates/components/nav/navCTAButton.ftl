[#--------------- RENDERING ---------------]
<li class="nav-item mx-3 text-center" style="background: white;">
    <a class="nav-link" href="${content.externalLink!}" target="_blank" style="color: black;">${content.title!}</a>
</li>