﻿[#--------------- INCLUDE ---------------]
[#import "/bestskopje-website/templates/macros/images.ftl" as image /]

[#--------------- RENDERING ---------------]
[#if content.images?has_content]
    <div id="carouselExampleIndicators" class="carousel slide carousel-fade mx-lg-5 mx-md-1 pb-5" data-ride="carousel" data-interval="3000">
        <ol class="carousel-indicators">
            [#list content.images as imageId]
                <li data-target="#carouselExampleIndicators" data-slide-to="${imageId?index}"[#if imageId?index==0] class="active"[/#if]></li>
            [/#list]
        </ol>
        <div class="carousel-inner">
            [#list content.images as imageId]
                    <div class="carousel-item[#if imageId?index==0] active[/#if]">
                        [@image.DAMImage imageUuid="${imageId}" imageStyleClass="d-block w-100"/]
                    </div>
            [/#list]
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
[/#if]