﻿[#--------------- INCLUDE ---------------]
[#import "/bestskopje-website/templates/macros/images.ftl" as image /]

[#--------------- ASSIGN ---------------]
[#assign partnersSectionFolder = cmsfn.contentById(content.partnersFolder, "partners")! /]
[#if partnersSectionFolder?has_content]
    [#assign partners = cmsfn.children(partnersSectionFolder)! /]
[/#if]

[#--------------- RENDERING ---------------]
[#if partners?has_content]
    <div class="row- mx-lg-5 mx-md-3 mx-sm-1 pt-3 text-center">
        <div class="h1">${content.title!}
            <hr />
        </div>
    </div>
    <div class="row mx-lg-5 mx-md-3 mx-sm-1">
        [#list partners as partner]
            [#assign partnerContentI18n = cmsfn.wrapForI18n(partner) /]
            [#if partnerContentI18n?has_content]
                <div class="col-lg-4 col-md-6 col-sm-12 mb-3 mx-auto">
                    <div class="card m-auto text-center bg-light border-0 rounded-lg h-100 shadow-lg"
                         style="max-width: 25rem;">
                        [@image.DAMImage imageUuid="${partnerContentI18n.logo!}" imageStyleClass="card-img-top img-fluid rounded mx-auto mt-4" imageAdditionalStyle="width: 90%;"/]
                        <div class="card-body d-flex flex-column">
                            <h5 class="card-title text-center mt-auto mb-0">${partnerContentI18n.entityName!}</h5>
                            [#if partnerContentI18n.websiteLink?has_content]
                                <a href="${partnerContentI18n.websiteLink!}" target="_blank"
                                   class="text-light mt-3 mx-auto"><span class="button-decoration">${i18n['bestskopje-website.global.website']}</span></a>
                            [/#if]
                        </div>
                    </div>
                </div>
            [/#if]
        [/#list]
    </div>
[/#if]