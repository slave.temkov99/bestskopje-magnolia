﻿[#--------------- RENDERING ---------------]
<div class="embed-responsive embed-responsive-16by9" style="height: 100vh; min-height: 500px;">
    ${cmsfn.decode(content).code!}
</div>