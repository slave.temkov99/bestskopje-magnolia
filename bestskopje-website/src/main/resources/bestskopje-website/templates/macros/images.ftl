﻿[#macro DAMImage imageUuid imageStyleClass="" imageAdditionalStyle=""]
    [#if imageUuid?has_content]
        [#assign asset = damfn.getAsset(imageUuid)! /]
        [#if asset?has_content]
            <img class="${imageStyleClass!}" style="${imageAdditionalStyle!}" src="${asset.link!}" alt="">
        [/#if]
    [/#if]
[/#macro]