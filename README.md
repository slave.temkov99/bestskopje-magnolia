# BEST Skopje project

BEST Skopje project is a Magnolia CMS project.

## Development setup

* Make sure you have JAVA 11 (LTS) version on your machine.

java -version

* Choose JAVA 11 sdk (jdk version: jdk-11.0.11.jdk) for the project in IntelliJ:
  File -> Project Structure -> Project Settings -> Project
* Download Tomcat 9.0.43 (recommended for Magnolia CMS 6.2)
* Set these VM options in the Tomcat configuration in IntelliJ:

-Xms64m
-Xmx1024m
-Dmagnolia.home="/path/to/your/webapp/deployment/directory"
-Dmagnolia.resources.dir="/path/to/your/filesystem/resources/lightmodule"